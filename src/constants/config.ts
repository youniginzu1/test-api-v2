require('dotenv').config();

export const configs = {
  SERVER: {
    PORT: Number(process.env.SERVER_PORT) || 3000,
  },
  MYSQL: {
    HOST: process.env.MYSQL_HOST,
    PORT: Number(process.env.MYSQL_PORT) || 3306,
    USER: process.env.MYSQL_USER,
    PASSWORD: process.env.MYSQL_PASSWORD,
    DB_NAME: process.env.MYSQL_DB_NAME,
  },
  AUTH: {
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
    JWT_ACCESS_TOKEN_EXPIRES_IN: process.env.JWT_ACCESS_TOKEN_EXPIRES_IN,
    JWT_REFRESH_TOKEN_EXPIRES_IN: process.env.JWT_REFRESH_TOKEN_EXPIRES_IN,
    BCRYPT_HASH_ROUNDS: Number(process.env.BCRYPT_HASH_ROUNDS) || 10,
  },
  UPLOAD: {
    S3_SECRET_KEY: process.env.S3_SECRET_KEY,
    S3_ACCESS_KEY: process.env.S3_ACCESS_KEY,
    S3_REGION: process.env.S3_REGION,
    S3_BUCKET: process.env.S3_BUCKET,
    THUMBS: process.env.THUMBS,
    MAX_FILES: Number(process.env.MAX_FILES) || 10,
    S3_DOMAIN: process.env.S3_DOMAIN,
  },
};
