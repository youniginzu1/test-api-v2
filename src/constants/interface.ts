import { TokenType } from './enum';

export interface ITokenPayload {
  id: number;
  tokenType: TokenType;
}
