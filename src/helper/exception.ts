import { HttpException, HttpStatus } from '@nestjs/common';

import { ErrorCode } from 'src/constants/enum';

export class CustomExceptionFactory extends HttpException {
  constructor(
    errorCode: ErrorCode,
    devMessage?: string | any,
    errorMessage?: string | any,
    statusCode?: HttpStatus,
    payload?: any
  ) {
    const errorObject = {
      errorCode,
      statusCode: statusCode || HttpStatus.BAD_REQUEST,
    };

    if (devMessage) errorObject['devMessage'] = devMessage;
    if (errorMessage) errorObject['errorMessage'] = errorMessage;
    if (payload) errorObject['payload'] = payload;

    super(errorObject, errorObject.statusCode);
  }
}

export class Exception extends CustomExceptionFactory {
  /**
   *
   * @example
   *
   *   throw Exception("Unknown_Error")
   *
   *   throw Exception("Unknown_Error", "This is error description")
   *
   *   throw Exception("Unknown_Error", "This is error description", HttpStatus.BAD_REQUEST)
   *
   *   throw Exception("Unknown_Error", "This is error description", HttpStatus.BAD_REQUEST, { isSystem: true })
   */
  constructor(
    errorCode: ErrorCode,
    devMessage?: string | any,
    errorMessage?: string | any,
    statusCode?: HttpStatus,
    payload?: any
  ) {
    super(errorCode, devMessage, errorMessage, statusCode, payload);
  }
}

export class Forbidden extends CustomExceptionFactory {
  /**
   *
   * @example
   *
   *    // Common forbidden error
   *    throw Forbidden()
   *
   *    // Forbidden with description message
   *    throw Forbidden("This is error description")
   *
   *    // Forbidden with description message & payload data
   *    throw Forbidden("This is error description", { payload: "This is payload" })
   */
  constructor(
    devMessage?: string | any,
    errorMessage?: string | any,
    payload?: any
  ) {
    super(
      ErrorCode.FORBIDDEN_RESOURCE,
      devMessage,
      errorMessage,
      HttpStatus.FORBIDDEN,
      payload
    );
  }
}

export class Unauthorized extends CustomExceptionFactory {
  /**
   *
   * @example
   *
   *    // Common forbidden error
   *    throw Unauthorized()
   *
   *    // Unauthorized with description message
   *    throw Unauthorized("This is error description")
   *
   *    // Unauthorized with description message & payload data
   *    throw Unauthorized("This is error description", { payload: "This is payload" })
   */
  constructor(
    devMessage?: string | any,
    errorMessage?: string | any,
    payload?: any
  ) {
    super(
      ErrorCode.UNAUTHORIZED,
      devMessage,
      errorMessage,
      HttpStatus.UNAUTHORIZED,
      payload
    );
  }
}

export class UnprocessableEntity extends CustomExceptionFactory {
  /**
   *
   * @example
   *
   *    // Common forbidden error
   *    throw UnprocessableEntity()
   *
   *    // UnprocessableEntity with description message
   *    throw UnprocessableEntity("This is error description")
   *
   *    // UnprocessableEntity with description message & payload data
   *    throw UnprocessableEntity("This is error description", { payload: "This is payload" })
   */
  constructor(
    devMessage?: string | any,
    errorMessage?: string | any,
    payload?: any
  ) {
    super(
      ErrorCode.INVALID_INPUT,
      devMessage,
      errorMessage,
      HttpStatus.UNPROCESSABLE_ENTITY,
      payload
    );
  }
}
