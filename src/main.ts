import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';
import { configs } from 'src/constants/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(configs.SERVER.PORT);
}
bootstrap();
