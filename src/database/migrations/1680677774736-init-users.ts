import { MigrationInterface, QueryRunner } from "typeorm";

export class initUsers1680677774736 implements MigrationInterface {
    name = 'initUsers1680677774736'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`users\` (\`id\` bigint UNSIGNED NOT NULL AUTO_INCREMENT, \`email\` varchar(255) NOT NULL, \`password\` varchar(100) NOT NULL, \`status\` enum ('ACTIVE', 'INACTIVE', 'NOT_VERIFY') NOT NULL DEFAULT 'ACTIVE', \`name\` varchar(255) NULL COMMENT 'Full name of the user.', \`phone\` varchar(10) NULL, \`avatar\` text NULL, \`refresh_token\` varchar(500) NULL, \`updated_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`created_at\` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6), \`deleted_at\` timestamp(6) NULL, UNIQUE INDEX \`IDX_97672ac88f789774dd47f7c8be\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_97672ac88f789774dd47f7c8be\` ON \`users\``);
        await queryRunner.query(`DROP TABLE \`users\``);
    }

}
