import { DataSource, DataSourceOptions } from 'typeorm';

import { configs } from 'src/constants/config';

export const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: configs.MYSQL.HOST,
  port: configs.MYSQL.PORT,
  username: configs.MYSQL.USER,
  password: configs.MYSQL.PASSWORD,
  database: configs.MYSQL.DB_NAME,
  entities: ['dist/database/entities/*.js'],
  migrations: ['dist/database/migrations/*.js'],
};

const dataSource = new DataSource(dataSourceOptions);
export default dataSource;
