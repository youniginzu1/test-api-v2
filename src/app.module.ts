import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_GUARD } from '@nestjs/core';

import { AuthModule } from './app/auth/auth.module';
import { JwtStrategy } from './core/strategies/jwt.strategy';
import { dataSourceOptions } from './database/data-source';
import { JwtAuthGuard } from './core/guards/jwt-auth.guard';
import { S3UploadModule } from './app/shared/s3-upload/s3-upload.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(dataSourceOptions),
    AuthModule,
    S3UploadModule,
  ],
  controllers: [],
  providers: [
    JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
