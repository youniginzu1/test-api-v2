import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthSharedModule } from '../shared/auth-shared/auth-shared.module';
import User from 'src/database/entities/users';

@Module({
  imports: [AuthSharedModule, TypeOrmModule.forFeature([User])],
  providers: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
