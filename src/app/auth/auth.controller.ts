import { Body, Controller, Get, Post } from '@nestjs/common';

import { AuthService } from './auth.service';
import { SignUpDto } from './dto/sign-up.dto';
import { Public } from 'src/core/decorators/role.decorator';
import { LoginDto } from './dto/login.dto';
import { validate } from 'src/helper/validate';
import { signUpSchema } from './schema/sign-up.schema';
import { loginSchema } from './schema/login.schema';
import { User } from 'src/core/decorators/user.decorator';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post('/sign-up')
  signUp(@Body() body: SignUpDto) {
    validate(signUpSchema, body);
    return this.authService.signUp(body);
  }

  @Public()
  @Post('/login')
  login(@Body() body: LoginDto) {
    validate(loginSchema, body);
    return this.authService.login(body);
  }

  @Get('/profile')
  getProfile(@User() user) {
    return this.authService.getProfile(user?.id);
  }
}
