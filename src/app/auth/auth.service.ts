import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { compare, hash } from 'bcrypt';

import { AuthSharedService } from '../shared/auth-shared/auth-shared.service';
import User from 'src/database/entities/users';
import { SignUpDto } from './dto/sign-up.dto';
import { Exception } from 'src/helper/exception';
import {
  ErrorCode,
  ErrorMessage,
  TokenType,
  UserStatus,
} from 'src/constants/enum';
import { configs } from 'src/constants/config';

@Injectable()
export class AuthService {
  constructor(
    private readonly authSharedService: AuthSharedService,
    @InjectRepository(User)
    private readonly userRepo: Repository<User>
  ) {}

  async generateToken(userRepository: Repository<User>, user: User) {
    const token = this.authSharedService.signToken({
      id: user.id,
      tokenType: TokenType.ACCESS_TOKEN,
    });
    const isValid = this.authSharedService.verifyToken(
      user.refreshToken,
      TokenType.REFRESH_TOKEN
    );

    if (!isValid) {
      const newRefreshToken = this.authSharedService.signToken({
        id: user.id,
        tokenType: TokenType.REFRESH_TOKEN,
      });
      await userRepository.update(user.id, { refreshToken: newRefreshToken });
      return { token, refreshToken: newRefreshToken };
    }

    return { token, refreshToken: user.refreshToken };
  }

  async signUp({ email, password }: SignUpDto) {
    const isEmailExist = await this.userRepo.findOne({
      where: { email },
      select: ['id'],
    });
    if (isEmailExist) {
      throw new Exception(
        ErrorCode.EMAIL_ALREADY_EXIST,
        'Email already exist. Chose an other one!',
        ErrorCode.EMAIL_ALREADY_EXIST
      );
    }

    const hashedPassword = await hash(
      password,
      configs.AUTH.BCRYPT_HASH_ROUNDS
    );
    const user = await this.userRepo.save({
      email,
      password: hashedPassword,
    });

    return await this.generateToken(this.userRepo, user);
  }

  async login({ email, password }) {
    const user = await this.userRepo.findOne({
      where: { email, status: UserStatus.ACTIVE },
      select: ['id', 'password', 'refreshToken'],
    });
    if (!user)
      throw new Exception(
        ErrorCode.EMAIL_OR_PASSWORD_INVALID,
        'Email invalid.',
        ErrorMessage.EMAIL_OR_PASSWORD_INVALID
      );

    const isValid = await compare(password, user.password);
    if (!isValid)
      throw new Exception(
        ErrorCode.EMAIL_OR_PASSWORD_INVALID,
        'Password invalid.',
        ErrorMessage.EMAIL_OR_PASSWORD_INVALID
      );

    return await this.generateToken(this.userRepo, user);
  }

  async getProfile(id: number) {
    return await this.userRepo.findOne({
      where: {
        id,
      },
    });
  }
}
