import { AjvSchema } from 'src/constants/AJV.schema';

export const signUpSchema: AjvSchema = {
  type: 'object',
  required: ['email', 'password'],
  additionalProperties: false,
  properties: {
    email: {
      format: 'email',
      type: 'string',
    },
    password: {
      type: 'string',
      minLength: 6,
      maxLength: 32,
    },
  },
};
