import { Controller, Post, UploadedFiles } from '@nestjs/common';
import * as md5 from 'md5';

import { configs } from 'src/constants/config';
import { Public } from 'src/core/decorators/role.decorator';
import { S3UploadService } from './s3-upload.service';
import { returnLoadMore } from 'src/helper';

@Controller('upload')
export class S3UploadController {
  constructor(private readonly s3UploadService: S3UploadService) {}

  @Post('/')
  @Public()
  async uploadFile(@UploadedFiles() files: Array<Express.Multer.File>) {
    const results = [];

    for (const file of files) {
      const arrExt = (file.originalname || '').split('.');
      const originalName = md5(file.originalname);
      const md5Name = arrExt.length
        ? `${originalName}.${arrExt[arrExt.length - 1]}`
        : originalName;
      const fileName = `${Date.now().toString()}-${md5Name}`;

      await this.s3UploadService.putImageToS3(file, fileName);
      results.push(fileName);
    }

    return returnLoadMore(results, {}, { domain: configs.UPLOAD.S3_DOMAIN });
  }
}
