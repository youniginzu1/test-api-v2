import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import * as Sharp from 'sharp';

import { configs } from 'src/constants/config';

const s3 = new AWS.S3({
  secretAccessKey: configs.UPLOAD.S3_SECRET_KEY,
  accessKeyId: configs.UPLOAD.S3_ACCESS_KEY,
  region: configs.UPLOAD.S3_REGION,
});

@Injectable()
export class S3UploadService {
  async putImageToS3(image: Express.Multer.File, fileName: string) {
    await s3
      .putObject({
        ACL: 'public-read',
        Body: image.buffer,
        Bucket: configs.UPLOAD.S3_BUCKET,
        ContentType: image.mimetype,
        Key: fileName,
      })
      .promise();
  }

  async generateThumb(image: Express.Multer.File, fileName: string) {
    const thumbs = configs.UPLOAD.THUMBS;

    for (let thumb of thumbs) {
      const [w, h] = thumb.split('x');
      let buffer = image.buffer;

      if (w && h) {
        buffer = await Sharp(image.buffer)
          .resize(Number(w), Number(h), {
            withoutEnlargement: true,
            fit: 'inside',
          })
          .toBuffer();

        if (!image['thumbs'] || !Array.isArray(image['thumbs']))
          image['thumbs'] = [];

        image['thumbs'].push({
          fileName: `${w}x${h}/${fileName}`,
          buffer,
        });
      }
    }
  }
}
