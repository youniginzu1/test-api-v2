import * as multer from 'multer';
import { MiddlewareConsumer, Module } from '@nestjs/common';

import { S3UploadService } from './s3-upload.service';
import { S3UploadController } from './s3-upload.controller';
import { configs } from 'src/constants/config';

const upload = multer();
@Module({
  providers: [S3UploadService],
  exports: [S3UploadService],
  controllers: [S3UploadController],
})
export class S3UploadModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(upload.array('files', configs.UPLOAD.MAX_FILES))
      .forRoutes(S3UploadController);
  }
}
