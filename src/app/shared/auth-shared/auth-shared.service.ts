import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { configs } from 'src/constants/config';
import { TokenType } from 'src/constants/enum';
import { ITokenPayload } from 'src/constants/interface';

@Injectable()
export class AuthSharedService {
  constructor(private readonly jwtService: JwtService) {}

  public signToken(payload: ITokenPayload): string {
    return this.jwtService.sign(payload, {
      secret: configs.AUTH.JWT_SECRET_KEY,
      expiresIn:
        payload.tokenType === TokenType.ACCESS_TOKEN
          ? configs.AUTH.JWT_ACCESS_TOKEN_EXPIRES_IN
          : configs.AUTH.JWT_REFRESH_TOKEN_EXPIRES_IN,
    });
  }

  public verifyToken(token: string, tokenType: TokenType) {
    try {
      const payload = this.jwtService.verify(token, {
        secret: configs.AUTH.JWT_SECRET_KEY,
      });
      return payload?.tokenType === tokenType ? payload : false;
    } catch (error) {
      return false;
    }
  }
}
