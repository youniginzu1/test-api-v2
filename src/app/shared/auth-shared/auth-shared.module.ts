import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { AuthSharedService } from './auth-shared.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [],
  providers: [AuthSharedService],
  exports: [AuthSharedService],
})
export class AuthSharedModule {}
