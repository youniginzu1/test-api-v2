import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { configs } from 'src/constants/config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configs.AUTH.JWT_SECRET_KEY,
    });
  }

  async validate(payload: any) {
    return {
      id: payload.id,
      tokenType: payload?.tokenType,
    };
  }
}
